####1. Information flow
i. vertical flow
ii. horizontal flow
####2. Contigency theory
 - Context + structure.
 - Context
   - external factors
   - strategy & goal
     - quality
     - quantity
   - Culture
     - creativity and innovation
   - Size / maturity
   - Human resources
     - skill level
####3. Functional structure
![](functional-org.png)
- Pros
  - Simple and easy to manage
  - People develop expertise in their functions.
  - Efficient and reduces duplication
  - Upholds standards and quality.
- Cons
  - Internally focussed.
  - hard to coordinate efforts between groups.
  - hard to see the big picture from within each function.
- Useful when
  - Small or medium firm
  - Efficiency and quality is the key to success over innovation.
####4. Divisional structure
#####4.1 Product or offering based division.
- Adaptability
  - tailors their offering based on the competitions in a given business.
  - divides the big company into smaller.
  - better coordination between functions.
- Cons
  - dvisions does not work in coordination.
  - lack of consistency and quality across divisions.
  - less efficient. duplication of efforts.
  - HBT and Aero have different tools and processes. They have differnt HR and finances.
#####4.2 Geographic based divisions.
#####4.3 Markets based divisions.

####5. Matrix structure.
**Honeywell is a divisional organization?**
![](boeing-matrix.png)
![](p&g-matrix.png)
- **PROS**
  - Integration of information and Decision
  - Two key factors based into decision making
    - Boeing - function and programs
    - P&G - Geography and products.
  - Crux of two dimension is the two boss manager.
  - The matrix structure makes sense to consider when companies face competing pressures on two broad dimensions.
- **CONS**
  - Conflict between the bosses - Robert Son Vs Vijay
  - Slow decisions - Agile is so late in Honeywell.
  - high cost
  - Issue with clarity of direction and accountability. - early v&v engagement.
- Use un-balanced matrix - strong and weak matrix. That helps to solve the deadlocks.
####6. Network
Company focusses on key areas and outsources the other functions.
- PROS
  - low Cost
  - allows firm to focus on its core competency.
- CONS
  - lack of Control
  - weaken employee loyalty.

####7. Team based / project based - Agile

####8. Holocracy - manager free

####9. Hybrid. 
